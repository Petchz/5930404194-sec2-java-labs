package banhanvoottikrai.taewapon.lab4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class OutPatient extends Patient{
		private LocalDate visitDate;
		public static String hospitalName = "Srinakarin ";
		
		DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(
		        FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
		
		public OutPatient(String name, String birthdateStr, Gender gender, double weight, int height) {
			super(name, birthdateStr, gender, weight, height);
		}
		
		public OutPatient(String name, String birthdateStr, Gender gender, double weight, int height, String visitDateStr) {
			super(name, birthdateStr, gender, weight, height);
			this.visitDate = LocalDate.parse(visitDateStr, germanFormatter);
		}

		public LocalDate getVisitDate() {
			return visitDate;
		}

		public void setVisitDate(String visitDate) {
			this.visitDate = LocalDate.parse(visitDate, germanFormatter);
		}
		
		public void displayDaysBetween(OutPatient individual) {
			long conclusion = getDaysCountBetweenDates(individual.getVisitDate(),visitDate);
			System.out.println("Chujai visited after " + individual.getName() + " for " + conclusion + " days.");
		}
		
		public long getDaysCountBetweenDates(LocalDate dateBefore, LocalDate dateAfter) {
		    return ChronoUnit.DAYS.between(dateBefore, dateAfter);
		}
}
