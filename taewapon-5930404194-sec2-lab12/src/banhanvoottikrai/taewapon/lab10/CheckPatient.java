package banhanvoottikrai.taewapon.lab10;

import banhanvoottikrai.taewapon.lab12.NoAddressException;
import banhanvoottikrai.taewapon.lab12.NoBirthdateException;
import banhanvoottikrai.taewapon.lab12.NoGenderException;
import banhanvoottikrai.taewapon.lab12.NoNameException;
import banhanvoottikrai.taewapon.lab4.Gender;

public class CheckPatient {

    public static String checkName(String name) throws NoNameException {
        if (name.isEmpty()) throw new NoNameException();
        return name;
    }

    public static Gender checkGender(boolean male, boolean female) throws NoGenderException {
        if (!(male || female)) throw new NoGenderException();
        if (male) return Gender.MALE;
        else return Gender.FEMALE;
    }

    public static String checkAddress(String address) throws NoAddressException {
        if (address.isEmpty()) throw new NoAddressException();
        return address;
    }

    public static String checkBirthdate(String birthdate) throws NoBirthdateException {
        if (birthdate.isEmpty()) throw new NoBirthdateException();
        return birthdate;
    }
}
