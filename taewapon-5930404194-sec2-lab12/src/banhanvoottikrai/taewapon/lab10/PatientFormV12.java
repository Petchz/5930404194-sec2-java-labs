package banhanvoottikrai.taewapon.lab10;

import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import banhanvoottikrai.taewapon.lab4.*;

public class PatientFormV12 extends PatientFormV11 implements Comparator<Patient>{
	
	// generated UID
	private static final long serialVersionUID = 4316161856118492040L;

	// actionPerformed method
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == searchMenuItem) {
			String searchName = 
					JOptionPane.showInputDialog("Please enter the patient name");
			searchPatient(searchName);
		} else if (src == removeMenuItem) {
			String removeName = 
					JOptionPane.showInputDialog("Please enter the patient name to remove");
			removePatient(removeName);
		}
	}
	
	// addListeners method
	public void addListeners() {
		super.addListeners();
		searchMenuItem.addActionListener(this);
		removeMenuItem.addActionListener(this);
	}
	
	// searchPatient method
	public void searchPatient (String searchName) {
		for (int i = 0 ; i < patientArrayList.size() ; i++) {
			if (searchName.equals(patientArrayList.get(i).getName())) {
				JOptionPane.showMessageDialog(this, patientArrayList.get(i).toString());
			}
			else if (i == patientArrayList.size() && !searchName.equals(patientArrayList.get(i).getName())) {
				JOptionPane.showMessageDialog(this, searchName + " is not found");
			}
		}
	}
	
	// removePatient method
	private void removePatient(String removeName) {
	for (int i = 0; i < patientArrayList.size(); i++) {
		if (patientArrayList.get(i).getName().equalsIgnoreCase(removeName)) {
			patientArrayList.remove(i);
		}
	}
	JOptionPane.showMessageDialog(this, removeName + " has removed ");
	}
	
	@Override
	public int compare(Patient arg0, Patient arg1) {
		return 0;
	}
	
	// title constructor
	public PatientFormV12(String title) {
		super(title);
	}
		
	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});
	}

	// createAndShowGUI method
	public static void createAndShowGUI() {
		PatientFormV12 patientV12 = new PatientFormV12("PatientFormV12");
		patientV12.addComponents();
		patientV12.setFrameFeatures();
		patientV12.addListeners();
	}
}

		
