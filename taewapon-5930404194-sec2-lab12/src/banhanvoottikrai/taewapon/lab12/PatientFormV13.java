package banhanvoottikrai.taewapon.lab12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import banhanvoottikrai.taewapon.lab10.PatientFormV12;
import banhanvoottikrai.taewapon.lab4.Gender;
import banhanvoottikrai.taewapon.lab4.Patient;

public class PatientFormV13 extends PatientFormV12 {

	// generated UID
	private static final long serialVersionUID = 1464818549462774634L;

	@Override
	// handleSaveMenuFileCS method
	protected void handleSaveMenuFileCS() {
		menuFileCS.setCurrentDirectory(new File(System.getProperty("user.home")));
		int result = menuFileCS.showSaveDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
		    File selectedFile = menuFileCS.getSelectedFile();
		    JOptionPane.showMessageDialog(this,"You have save patient data to file " + selectedFile.getName());
		    
		    try {
		    	String filename = menuFileCS.getSelectedFile().toString();
		    	PrintWriter writer = new PrintWriter (filename);

		    	for (int i = 0; i < patientArrayList.size(); i++){
		    		writer.println("Name = " + patientArrayList.get(i).getName());
		    		writer.println("Birthdate = " + patientArrayList.get(i).getBirthdate());
		    		writer.println("Weight = " + patientArrayList.get(i).getWeight());
		    		writer.println("Height = " + patientArrayList.get(i).getHeight());
		    		writer.println("Gender = " + patientArrayList.get(i).getGender());
		    	}
		    	writer.close();
		    } catch (IOException ex){
		    	System.err.println("Error in writing file");
		    	ex.printStackTrace(System.err);
		    }
		}
	}
	
	@Override
	// handleOpenMenuFileCS method
	protected void handleOpenMenuFileCS() {
		menuFileCS.setCurrentDirectory(new File(System.getProperty("user.home")));
		int result = menuFileCS.showOpenDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
		    File selectedFile = menuFileCS.getSelectedFile();
		    JOptionPane.showMessageDialog(this,"You have opened file " + selectedFile.getName());
		    
		    try {
	            FileReader fileReader = new FileReader(selectedFile);
	            BufferedReader bufferedReader = new BufferedReader(fileReader);
	            String line;
	            DateTimeFormatter fileReaderFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	            while ((line = bufferedReader.readLine()) != null) {
	                String[] str = line.split(" = ");
	                String name = str[1];

	                line = bufferedReader.readLine();
	                str = line.split(" = ");
	                String birthdate = str[1];

	                line = bufferedReader.readLine();
	                str = line.split(" = ");
	                double weight = Double.parseDouble(str[1]);

	                line = bufferedReader.readLine();
	                str = line.split(" = ");
	                int height = Integer.parseInt(str[1]);

	                line = bufferedReader.readLine();
	                str = line.split(" = ");
	                Gender gender;
	                if (str[1].equals("MALE")) {
	                    gender = Gender.MALE;
	                } else {
	                    gender = Gender.FEMALE;
	                }

	                Patient patientToAdd;
	                patientToAdd = new Patient(name, birthdate, gender, weight,height);
	                patientArrayList.add(patientToAdd);
	            }
	            bufferedReader.close();
	            fileReader.close();
	        } catch (IOException ex) {
	            System.err.println("Error while reading");
	            ex.printStackTrace(System.err);
	        }
		}
	}

	// title constructor
	public PatientFormV13(String title) {
		super(title);
	}
		
	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});
	}

	// createAndShowGUI method
	public static void createAndShowGUI() {
		PatientFormV13 patientV13 = new PatientFormV13("Patient Form V13");
		patientV13.addComponents();
		patientV13.setFrameFeatures();
		patientV13.addListeners();
	}
}
