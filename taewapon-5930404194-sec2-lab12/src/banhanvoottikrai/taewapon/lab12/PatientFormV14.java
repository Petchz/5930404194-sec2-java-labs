package banhanvoottikrai.taewapon.lab12;

import javax.swing.*;

import banhanvoottikrai.taewapon.lab10.CheckPatient;
import banhanvoottikrai.taewapon.lab4.Gender;
import banhanvoottikrai.taewapon.lab4.Patient;

import java.time.format.DateTimeParseException;


public class PatientFormV14 extends PatientFormV13 {

	// generated UID
	private static final long serialVersionUID = 8236870859153522967L;

	@Override
    public void addListeners() {
        super.addListeners();
        okButton.removeActionListener(okButton.getActionListeners()[0]);
        okButton.addActionListener(this);
    }

    @Override
    public void handleOKButton() {
        Patient patientToAdd;

        try {
            String name = CheckPatient.checkName(nameTextfield.getText());
            String birthDate = CheckPatient.checkBirthdate(birthdateTextfield.getText());
            double weight = Double.parseDouble(weightTextfield.getText());
            int height = Integer.parseInt(heightTextfield.getText());
            Gender gender = CheckPatient.checkGender(maleRadioButton.isSelected(), femaleRadioButton.isSelected());
            CheckPatient.checkAddress(addressTextArea.getText());

            patientToAdd = new Patient(name, birthDate,gender, weight, height);
            patientArrayList.add(patientToAdd);

            super.handleOKButton();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Please enter weight in double and height in int");
        } catch (DateTimeParseException e) {
            JOptionPane.showMessageDialog(this, "Please enter Birthdate in the format DD.MM.YYYY ex. 22.02.2000");
        } catch (NoNameException e) {
            JOptionPane.showMessageDialog( this, "Name has not been entered.");
        } catch (NoGenderException e) {
            JOptionPane.showMessageDialog( this, "No gender has been selected.");
        } catch (NoAddressException e) {
            JOptionPane.showMessageDialog( this, "Address has not been entered.");
        } catch (NoBirthdateException e) {
            JOptionPane.showMessageDialog( this, "Please enter birthdate.");
        }
    }
    
	// title constructor
	public PatientFormV14(String title) {
		super(title);
	}
		
	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});
	}
    
	// createAndShowGUI method
	public static void createAndShowGUI() {
		PatientFormV13 patientV13 = new PatientFormV13("Patient Form V14");
		patientV13.addComponents();
		patientV13.setFrameFeatures();
		patientV13.addListeners();
	}
}
