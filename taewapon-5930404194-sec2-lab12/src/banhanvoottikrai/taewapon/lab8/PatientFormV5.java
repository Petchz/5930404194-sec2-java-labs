package banhanvoottikrai.taewapon.lab8;

/**
 * PatientFormV5 Program
 * 
 * @author Taewapon Banhanvoottikrai 593040419-4
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class PatientFormV5 extends PatientFormV4 {
	
	// generated UID
	private static final long serialVersionUID = 3052023378319821620L;

	// declare variable
	protected JMenu colorMenu,sizeMenu;
	protected JMenuItem blueMenuItem,greenMenuItem,redMenuItem,customcolorMenuItem,
						size16MenuItem,size20MenuItem,size24MenuItem,customsizeMenuItem;
	protected Font font16,font20,font24;
	
	// title Constructor
	public PatientFormV5(String title) {
		super(title);
	}

	protected void addComponents(){
		super.addComponents();
		
		// set variable
		colorMenu = new JMenu("Color");
		sizeMenu = new JMenu("Size");
		
		font16 = new Font("SansSerif",Font.BOLD,16);
		font20 = new Font("SansSerif",Font.BOLD,20);
		font24 = new Font("SansSerif",Font.BOLD,24);
		
		// set colorMenuItem variable
		blueMenuItem = new JMenuItem("Blue");
		greenMenuItem = new JMenuItem("Green");
		redMenuItem = new JMenuItem("Red");
		customcolorMenuItem = new JMenuItem("Custom...");
		
		// set sizeMenuItem variable
		size16MenuItem = new JMenuItem("16");
		size20MenuItem = new JMenuItem("20");
		size24MenuItem = new JMenuItem("24");
		customsizeMenuItem = new JMenuItem("Custom...");
		
		// remove old MenuItem
		configMenu.remove(colorMenuItem);
		configMenu.remove(sizeMenuItem);
		
		// set colorMenu
		colorMenu.add(blueMenuItem);
		colorMenu.add(greenMenuItem);
		colorMenu.add(redMenuItem);
		colorMenu.add(customcolorMenuItem);
		configMenu.add(colorMenu);
		
		// set sizeMenu
		sizeMenu.add(size16MenuItem);
		sizeMenu.add(size20MenuItem);
		sizeMenu.add(size24MenuItem);
		sizeMenu.add(customsizeMenuItem);
		configMenu.add(sizeMenu);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		
		Object src = event.getSource();
		if (src == blueMenuItem) {
			handlebluecolor();
		} else if (src == greenMenuItem) {
			handlegreencolor();
		} else if (src == redMenuItem){
			handleredcolor();
		} else if (src == size16MenuItem){
			handlesize16();
		} else if (src == size20MenuItem){
			handlesize20();
		} else if (src == size24MenuItem)
			handlesize24();
	}
	
	// set font color methods
	private void handlebluecolor() {
		nameTextfield.setForeground(Color.BLUE);
		birthdateTextfield.setForeground(Color.BLUE);
		weightTextfield.setForeground(Color.BLUE);
		heightTextfield.setForeground(Color.BLUE);
		addressTextArea.setForeground(Color.BLUE);
	}
	private void handlegreencolor() {
		nameTextfield.setForeground(Color.GREEN);
		birthdateTextfield.setForeground(Color.GREEN);
		weightTextfield.setForeground(Color.GREEN);
		heightTextfield.setForeground(Color.GREEN);
		addressTextArea.setForeground(Color.GREEN);
	}	
	private void handleredcolor() {
		nameTextfield.setForeground(Color.RED);
		birthdateTextfield.setForeground(Color.RED);
		weightTextfield.setForeground(Color.RED);
		heightTextfield.setForeground(Color.RED);
		addressTextArea.setForeground(Color.RED);
	}
	
	// set font size methods
	private void handlesize16() {
		nameTextfield.setFont(font16);
		birthdateTextfield.setFont(font16);
		weightTextfield.setFont(font16);
		heightTextfield.setFont(font16);
		addressTextArea.setFont(font16);
	}
	private void handlesize20() {
		nameTextfield.setFont(font20);
		birthdateTextfield.setFont(font20);
		weightTextfield.setFont(font20);
		heightTextfield.setFont(font20);
		addressTextArea.setFont(font20);
	}
	private void handlesize24() {
		nameTextfield.setFont(font24);
		birthdateTextfield.setFont(font24);
		weightTextfield.setFont(font24);
		heightTextfield.setFont(font24);
		addressTextArea.setFont(font24);
	}

	// addListeners method
	protected void addListeners() {
		super.addListeners();
		
		// color listener
		blueMenuItem.addActionListener(this);
		greenMenuItem.addActionListener(this);
		redMenuItem.addActionListener(this);
		
		// size listener
		size16MenuItem.addActionListener(this);
		size20MenuItem.addActionListener(this);
		size24MenuItem.addActionListener(this);
	}

	// createAndShowGUI method
	public static void createAndShowGUI() {
		PatientFormV5 patientForm5 = new PatientFormV5("Patient Form V5");
		patientForm5.addComponents();
		patientForm5.setFrameFeatures();
		patientForm5.addListeners();
	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}