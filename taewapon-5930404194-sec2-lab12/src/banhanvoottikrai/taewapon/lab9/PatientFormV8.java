package banhanvoottikrai.taewapon.lab9;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class PatientFormV8 extends PatientFormV7 {

	// generated UID
	private static final long serialVersionUID = -306374483656746162L;

	// addComponents method
	public void addComponents() {
		super.addComponents();
		
		// set Mnemonic keys for fileMenu and fileMenuItem
		fileMenu.setMnemonic(KeyEvent.VK_F);
		newMenuItem.setMnemonic(KeyEvent.VK_N);
		openMenuItem.setMnemonic(KeyEvent.VK_O);
		saveMenuItem.setMnemonic(KeyEvent.VK_S);
		exitMenuItem.setMnemonic(KeyEvent.VK_X);
		
		// set Mnemonic keys for configMenu and configMenuItem
		configMenu.setMnemonic(KeyEvent.VK_C);
		colorMenu.setMnemonic(KeyEvent.VK_L);
		blueMenuItem.setMnemonic(KeyEvent.VK_B);
		greenMenuItem.setMnemonic(KeyEvent.VK_G);
		redMenuItem.setMnemonic(KeyEvent.VK_R);
		customcolorMenuItem.setMnemonic(KeyEvent.VK_U);
		sizeMenu.setMnemonic(KeyEvent.VK_Z);
		size16MenuItem.setMnemonic(KeyEvent.VK_6);
		size20MenuItem.setMnemonic(KeyEvent.VK_0);
		size24MenuItem.setMnemonic(KeyEvent.VK_4);
		customsizeMenuItem.setMnemonic(KeyEvent.VK_M);
		
		// set Accelerator keys for fileMenuItem
		newMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		openMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		saveMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		exitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
		
		// set Accelerator keys for configMenuItem
		blueMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
		greenMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
		redMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		customcolorMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));
		
		size16MenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_6, ActionEvent.CTRL_MASK));
		size20MenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_0, ActionEvent.CTRL_MASK));
		size24MenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, ActionEvent.CTRL_MASK));
		customsizeMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, ActionEvent.CTRL_MASK));
	}
	
	// title constructor
	public PatientFormV8(String title) {
		super(title);
	}
	
	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		PatientFormV8 patientForm8 = new PatientFormV8("Patient Form V8");
		patientForm8.addComponents();
		patientForm8.setFrameFeatures();
		patientForm8.addListeners();
	}
	
}
