package banhanvoottikrai.taewapon.lab9;

import java.awt.*;
import java.awt.event.ActionEvent;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import banhanvoottikrai.taewapon.lab8.PatientFormV6;

public class PatientFormV7 extends PatientFormV6 implements ChangeListener{

	// generated UID
	private static final long serialVersionUID = -2450484647779064296L;
	
	// declare variable
	protected JSlider topNumSlider, bottomNumSlider;
	protected JLabel topNumLabel, bottomNumLabel;
	protected JPanel patientv7Panel, bloodPanel, northPanel, centerPanel, southPanel ;
	protected TitledBorder bloodPanelTitle;
	
	// set variable in this method
	private void setVariable() {
		
		// setup for topNumberSlider
		topNumSlider = new JSlider (JSlider.HORIZONTAL,0 , 200, 100);
		topNumSlider.setName("Top number of Blood Pressure");
		topNumSlider.setMinorTickSpacing(10);
		topNumSlider.setMajorTickSpacing(50);
		topNumSlider.setPaintTicks(true);
		topNumSlider.setPaintLabels(true);
	    
		// setup for bottomNumberSlider
		bottomNumSlider = new JSlider (JSlider.HORIZONTAL,0 , 200, 100);
		bottomNumSlider.setName("Bottom number of Blood Pressure");
		bottomNumSlider.setMinorTickSpacing(10);
		bottomNumSlider.setMajorTickSpacing(50);
	    bottomNumSlider.setPaintTicks(true);
	    bottomNumSlider.setPaintLabels(true);
		
	    // set text for topNumber and bottomNumber
		topNumLabel = new JLabel ("Top number:");
		bottomNumLabel = new JLabel ("Bottom number:");
		
		// setup all JPanel
		bloodPanel = new JPanel (new GridLayout(0, 2));
		northPanel = new JPanel (new BorderLayout());
		centerPanel = new JPanel (new BorderLayout());
		southPanel = new JPanel (new BorderLayout());
		patientv7Panel = new JPanel (new BorderLayout());

		// set bloodPanelTitle title to Blood Pressure
		bloodPanelTitle = BorderFactory.createTitledBorder("Blood Pressure");
		
	}
	
	// addComponents method
	@Override
	public void addComponents() {
		super.addComponents();
		
		// set variable
		setVariable();
		
		// add components to bloodPanel
		bloodPanel.setBorder(bloodPanelTitle);
		bloodPanel.add(topNumLabel);
		bloodPanel.add(topNumSlider);
		bloodPanel.add(bottomNumLabel);
		bloodPanel.add(bottomNumSlider);
		
		// add component to northPanel
		northPanel.add(patientMenuBar, BorderLayout.NORTH);
		northPanel.add(picPanel, BorderLayout.CENTER);
		northPanel.add(patientPanel, BorderLayout.SOUTH);
		
		// add component to centerPanel
		centerPanel.add(genderPanel, BorderLayout.NORTH);
		centerPanel.add(bloodPanel, BorderLayout.SOUTH);
		
		// add component to southPanel
		southPanel.add(addressPanel, BorderLayout.NORTH);
		southPanel.add(patienttypePanel, BorderLayout.CENTER);
		southPanel.add(mswPanel, BorderLayout.SOUTH);
		
		// add north, center and south panel to patientv7panel
		patientv7Panel.add(northPanel, BorderLayout.NORTH);
		patientv7Panel.add(centerPanel, BorderLayout.CENTER);
		patientv7Panel.add(southPanel, BorderLayout.SOUTH);
		
		// add patientv7Panel to JFrame
		add(patientv7Panel);
		
	}

	// actionPerformed method
	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
	}
	
	// stateChange method
	@Override
	public void stateChanged(ChangeEvent event) {
		JSlider src = (JSlider)event.getSource();
		 if (!src.getValueIsAdjusting()) {
	            int slideValue = (int)src.getValue();
	            JOptionPane.showMessageDialog(this, src.getName() + " is " + slideValue);
		 }
	}
	
	// addListeners method
	protected void addListeners() {
		super.addListeners();
		
		// add topNumber and bottomNumber Listener
		topNumSlider.addChangeListener(this);
		bottomNumSlider.addChangeListener(this);
	}

	// handleOKButton
	public void handleOKButton() {
		JOptionPane.showMessageDialog(this, "Name = " + nameTextfield.getText() + " " + 
				"Birthdate = " + birthdateTextfield.getText() + " " +
				"Weight = " + weightTextfield.getText() + " " +
				"Height = " + heightTextfield.getText() +  "\n" +
				"Gender = " + (maleRadioButton.isSelected() ? "Male" : 
								femaleRadioButton.isSelected() ? "Female":"") + "\n" +
				"Address = " + addressTextArea.getText() + "\n" +
				"Type = " + patienttypeComboBox.getSelectedItem() + "\n" +
				"Blood Pressure : Top Number = " + topNumSlider.getValue() + 
								" Bottom Number = " + bottomNumSlider.getValue());
	}
	
	// title constructor
	public PatientFormV7(String title) {
		super(title);
	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		PatientFormV7 patientForm7 = new PatientFormV7("Patient Form V7");
		patientForm7.addComponents();
		patientForm7.setFrameFeatures();
		patientForm7.addListeners();
	}
	
}