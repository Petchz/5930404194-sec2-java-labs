package banhanvoottikrai.taewapon.lab9;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.*;
import javax.swing.*;

public class PatientFormV9 extends PatientFormV8{

	// generated UID
	private static final long serialVersionUID = 1788855387858916359L;

	// declare variable
	protected JFileChooser menuFileCS = new JFileChooser ();;
	protected Color colorCS, previouslyColor;
	
	// actionPerformed method
	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		
		if (event.getSource() == openMenuItem) {
			handleOpenMenuFileCS();
		} else if (event.getSource() == saveMenuItem) {
			handleSaveMenuFileCS();
		} else if (event.getSource() == exitMenuItem) {
			System.exit(0);
		} else if (event.getSource() == customcolorMenuItem) {
			handleColorCS();
		}
	}

	// addListeners method
	protected void addListeners() {
		super.addListeners();
		
		// add openMenuItem and saveMenuItem Listener
		openMenuItem.addActionListener(this);
		saveMenuItem.addActionListener(this);
		exitMenuItem.addActionListener(this);
		customcolorMenuItem.addActionListener(this);
	}
	
	// handleOpenMenuFileCS method
	protected void handleOpenMenuFileCS() {
		menuFileCS.setCurrentDirectory(new File(System.getProperty("user.home")));
		int result = menuFileCS.showOpenDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
		    File selectedFile = menuFileCS.getSelectedFile();
		    JOptionPane.showMessageDialog(this,"You have opened file " + selectedFile.getName());
		} else if (result == JFileChooser.CANCEL_OPTION) {
			JOptionPane.showMessageDialog(this,"Open command is cancelled");
		}
	}
	
	// handleSaveMenuFileCS method
	protected void handleSaveMenuFileCS() {
		menuFileCS.setCurrentDirectory(new File(System.getProperty("user.home")));
		int result = menuFileCS.showSaveDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
		    File selectedFile = menuFileCS.getSelectedFile();
		    JOptionPane.showMessageDialog(this,"You have save file " + selectedFile.getName());
		} else if (result == JFileChooser.CANCEL_OPTION) {
			JOptionPane.showMessageDialog(this,"Save command is cancelled");
		}
	}
	
	// handleColorCS method
	protected void handleColorCS() {
		// get previuoslyColor from addressTextArea
		previouslyColor = addressTextArea.getForeground();
		
		// get colorCS variable to ColorChooser
		colorCS = JColorChooser.showDialog(this,"Select a color",previouslyColor);
		
		// set color from colorCS for all TextField
		nameTextfield.setForeground(colorCS);
		birthdateTextfield.setForeground(colorCS);
		weightTextfield.setForeground(colorCS);
		heightTextfield.setForeground(colorCS);
		addressTextArea.setForeground(colorCS);
		
	}

	// title constructor
	public PatientFormV9(String title) {
		super(title);
	}
	
	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		PatientFormV9 patientForm9 = new PatientFormV9("Patient Form V8");
		patientForm9.addComponents();
		patientForm9.setFrameFeatures();
		patientForm9.addListeners();
	}
	
}
