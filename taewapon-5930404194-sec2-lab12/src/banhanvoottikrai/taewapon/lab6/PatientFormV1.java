package banhanvoottikrai.taewapon.lab6;

/**
 * PatientFormV1 Program
 * 
 * @author Taewapon Banhanvoottikrai 593040419-4
 */

import java.awt.*;
import javax.swing.*;


public class PatientFormV1 extends MySimpleWindow {

	// serial UID
	private static final long serialVersionUID = 7873474954959974415L;

	// declare variable
	protected JPanel patientPanel, totalPanel;
	protected JLabel nameLabel, birthdateLabel, weightLabel, heightLabel;
	protected JTextField nameTextfield, birthdateTextfield, weightTextfield, heightTextfield;
	protected final int TEXTFIELD_LENGTH = 15;

	// title constructor
	public PatientFormV1(String title) {
		super(title);
	}

	@Override
	protected void addComponents() {
		super.addComponents();

		// setup JPanel
		patientPanel = new JPanel();
		totalPanel = new JPanel();

		// setup JLabel
		nameLabel = new JLabel("Name: ");
		birthdateLabel = new JLabel("Birthdate: ");
		weightLabel = new JLabel("Weight (kg.): ");
		heightLabel = new JLabel("Height (metre): ");

		// setup TEXTFIELD_LENGTH
		nameTextfield = new JTextField(TEXTFIELD_LENGTH);
		birthdateTextfield = new JTextField(TEXTFIELD_LENGTH);
		weightTextfield = new JTextField(TEXTFIELD_LENGTH);
		heightTextfield = new JTextField(TEXTFIELD_LENGTH);

		// setup ToolTipText
		birthdateTextfield.setToolTipText("ex. 22.02.2000");

		// setup Layout
		patientPanel.setLayout(new GridLayout(0, 2));
		totalPanel.setLayout(new BorderLayout());

		// add JLabel and JTextField to JPanel
		patientPanel.add(nameLabel);
		patientPanel.add(nameTextfield);
		patientPanel.add(birthdateLabel);
		patientPanel.add(birthdateTextfield);
		patientPanel.add(weightLabel);
		patientPanel.add(weightTextfield);
		patientPanel.add(heightLabel);
		patientPanel.add(heightTextfield);

		// add PatientPanel, mswPanel totalPanel and set Layout for both Panel
		totalPanel.add(patientPanel, BorderLayout.NORTH);
		totalPanel.add(mswPanel, BorderLayout.SOUTH);

		// add JPanel to Frame
		add(totalPanel);

	}

	// createAndShowGUI method
	public static void createAndShowGUI() {
		PatientFormV1 patientForm1 = new PatientFormV1("Patient Form V1");
		patientForm1.addComponents();
		patientForm1.setFrameFeatures();
	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}