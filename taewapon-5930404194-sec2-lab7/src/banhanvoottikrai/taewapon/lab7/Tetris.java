package banhanvoottikrai.taewapon.lab7;

/**
 * Tetris
 * 
 * This program is create 7 tetrimino pieces and word "Tetris"
 * 
 * @author Taewapon Banhanvoottikrai
 * 
 * @version 1.0
 *
 */

import java.awt.*;
import javax.swing.*;

public class Tetris extends JFrame {
	
	// generated UID
	private static final long serialVersionUID = -1447928170253961171L;
	
	TetrisPanel tetris;
	
	// title Constructor
	public Tetris(String title) {
		super(title);
	}
	
	// addComponents method
	public void addComponents() {
		tetris = new TetrisPanel();
		add(tetris);
	}

	// setFrameFeatures Method
	protected void setFrameFeatures() {
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setVisible(true);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	// createAndShowGUI Method
	public static void createAndShowGUI() {
		Tetris tetris = new Tetris("CoE Tetris Game");		// The title of the program 
		tetris.addComponents();
		tetris.setFrameFeatures();
	}
	
	// main method
	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
