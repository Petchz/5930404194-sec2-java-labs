package banhanvoottikrai.taewapon.lab7;

/**
 * TetrisV2
 * 
 * This class is create block and random for dropping
 * 
 * @author Taewapon Banhanvoottikrai
 * 
 * @version 1.0
 *
 */

import javax.swing.SwingUtilities;

public class TetrisV2 extends Tetris {
	
	//generated UID
	private static final long serialVersionUID = -3255795927625446312L;
	
	TetrisPanelV2 tetrisPanel2;

	public TetrisV2(String title) {
		super(title);

	}
	
	// main 
	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	// addComponents Method
	public void addComponents() {
		tetrisPanel2 = new TetrisPanelV2();
		add(tetrisPanel2);
	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		TetrisV2 tetris2 = new TetrisV2("Rectangle Dropping");
		tetris2.addComponents();
		tetris2.setFrameFeatures();
	}

}
