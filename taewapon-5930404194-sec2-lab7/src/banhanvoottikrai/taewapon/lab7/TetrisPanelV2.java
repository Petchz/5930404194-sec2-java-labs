package banhanvoottikrai.taewapon.lab7;

/**
 * TetrisPanelV2
 * 
 * This class use in Tetris V2
 * 
 * @author Taewapon Banhanvoottikrai
 * 
 * @version 1.0
 *
 */

import java.awt.Color;
import java.awt.Graphics;

public class TetrisPanelV2 extends TetrisPanel implements Runnable {
	
	// generated UID
	private static final long serialVersionUID = 2996871457081409544L;
	
	// create variable
	protected final static int RECT_WIDTH = 50;
	protected final static int SPEED = 10;
	int x, y;
	Thread running;
	
	// Constructor
	public TetrisPanelV2() {
		super();
		setBackground(Color.BLACK);		// set color for background
		running = new Thread(this);
		running.start();
	}

	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.YELLOW);					// set color for fill
		g.fillRect(x, y, RECT_WIDTH, RECT_WIDTH);	// fill color in rectangle

		repaint();
	}

	@Override
	public void run() {
		while (true) {
			if (y >= HEIGHT + 1) {
				x = 0 + (int) (Math.random() * ((600 - 0) + 1));	// random line for falling

				y = 0;
			}
			y += SPEED;

			try {
				Thread.sleep(25);
			} catch (InterruptedException ex) {

			}
		}

	}
}
