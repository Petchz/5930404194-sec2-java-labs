package banhanvoottikrai.taewapon.lab7;

/**
 * TetrisV3
 * 
 * This class is about rectangle random falling and not disappear
 *
 * @author Taewapon Banhanvoottikrai
 * 
 * @version 1.0
 *
 */

import javax.swing.SwingUtilities;

public class TetrisV3 extends TetrisV2 {
	
	// generated UID
	private static final long serialVersionUID = -1504187744401653517L;

	public TetrisV3(String title) {
		super(title);
	}

	TetrisPanelV3 tetrisPanel3;
	
	// main
	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	// addComponents Method
	public void addComponents() {
		tetrisPanel3 = new TetrisPanelV3();
		add(tetrisPanel3);
	}
	
	// createAndShowGUI Method
	public static void createAndShowGUI() {
		TetrisV3 tetris3 = new TetrisV3("Rectangle Dropping V2");
		tetris3.addComponents();
		tetris3.setFrameFeatures();
	}
}
