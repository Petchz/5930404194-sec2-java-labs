package banhanvoottikrai.taewapon.lab4;

/**
 * KhonKaenPatientsV2
 * 
 * This program is the examples of how to create objects in
 * class OutPatient
 * 
 * @author Taewapon B.
 * 
 * @version 1.1.0
 * 
 * 14-02-2017
 *
 */

public class KhonKaenPatientsV2 {

	public static void main(String[] args) {
		OutPatient chujai = new OutPatient("Chujai", "02.06.1995", Gender.FEMALE, 52.7, 150);
		OutPatient piti = new OutPatient("Piti", "13.08.1995", Gender.MALE, 52.7, 150, "21.01.2017");
		chujai.setVisitDate("25.01.2017");
		System.out.println(chujai);
		System.out.println(piti);
		chujai.displayDaysBetween(piti);
		System.out.println("Both of them went to " + OutPatient.hospitalName + "hospital.");
	}
}
