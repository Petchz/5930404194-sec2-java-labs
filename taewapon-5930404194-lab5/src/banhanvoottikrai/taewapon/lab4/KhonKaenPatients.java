package banhanvoottikrai.taewapon.lab4;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * KhonKaenPatients
 * 
 * This program is to illustrate the examples of how to create objects in
 * class Patient
 * 
 * @author Taewapon B.
 * 
 * @version 1.1.0
 * 
 * 14-02-2017
 *
 */

public class KhonKaenPatients {

	public static void main(String[] args) {
		Patient mana = new Patient("Mana","20.01.1990",Gender.MALE, 58.7, 160);
		Patient manee = new Patient("Manee","12.02.1995",Gender.FEMALE, 52.7, 150);
		mana.setWeight(60.7);
		LocalDate today = LocalDate.now();
		long yearsDelta = mana.getBirthdate().until(today, ChronoUnit.YEARS);
		System.out.println("Mana is " + yearsDelta + " years old");
		System.out.println("Manee's height is " + manee.getHeight() + " cm.");
		System.out.println(mana);
		System.out.println(manee);
	}

}

