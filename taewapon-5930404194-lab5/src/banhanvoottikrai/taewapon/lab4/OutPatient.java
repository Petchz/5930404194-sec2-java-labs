package banhanvoottikrai.taewapon.lab4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class OutPatient extends Patient {

	// Private Variable
	private LocalDate VisitDate;
	
	// Static Variable
	public static String hospitalName = "Srinakarin ";
	
	// DatetimeForrmatter
	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
	
	// InPatient Constructor
	public OutPatient(String SName, String SBirthDate, Gender SGender, double SWeight, int SHeight) {
		super(SName, SBirthDate, SGender, SWeight, SHeight);
	}
	
	// OutPatient Constructor
	public OutPatient(String SName, String SBirthDate, Gender SGender, double SWeight, int SHeight,String SVisitdate) {
		super(SName, SBirthDate, SGender, SWeight, SHeight);
		this.VisitDate = LocalDate.parse(SVisitdate, germanFormatter);
	}

	// Getters and setters (setVisitDate)
	public void setVisitDate(String SVisitdate) {
		this.VisitDate = LocalDate.parse(SVisitdate, germanFormatter);
	}
	
	public LocalDate getVisitDate() {
		return this.VisitDate;
	}
	
	// Getters and setters (HospitalName)
	public static void setHospitalName(String ShospitalName) {
		OutPatient.hospitalName = ShospitalName;
	}

	public static String getHospitalName() {
		return hospitalName;
	}


	// DisplaybetweenDate
	public void displayDaysBetween(OutPatient individual) {
		long conclusion = getDaysCountBetweenDates(individual.getVisitDate(), VisitDate);
		System.out.println("Chujai visited after " + individual.getName() + " for " + conclusion + " days.");
	}

	public long getDaysCountBetweenDates(LocalDate dateBefore, LocalDate dateAfter) {
		return ChronoUnit.DAYS.between(dateBefore, dateAfter);
	}
	
	@Override
	public String toString(){
		return "OutPatient [" + this.getName() + ", birthdate = " 
							+ this.getBirthdate() + ", "
							+ this.getGender() + ", "
							+ this.getWeight() + " kg., "
							+ this.getHeight() + " cm. visitdate = "
							+ this.getVisitDate() + "]";
				
	}
}
