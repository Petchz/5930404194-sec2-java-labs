package banhanvoottikrai.taewapon.lab4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class InPatient extends Patient {
	
	// Private Variable
	private LocalDate admitDate;
	private LocalDate dischargeDate;
	
	// DateTimeformatter
	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMAN);

	// Constructor
	public InPatient(String SName, String SBirthDate, Gender SGender, double SWeight, int SHeight, String SAdmitdate,String SDischargeDate) {
		super(SName, SBirthDate, SGender, SWeight, SHeight);
		this.admitDate = LocalDate.parse(SAdmitdate, germanFormatter);
		this.dischargeDate = LocalDate.parse(SDischargeDate, germanFormatter);
	}

	// Getters and setters (AdmitDate)
	public void setAdmiteDate(String SAdmitDate) {
		this.admitDate = LocalDate.parse(SAdmitDate, germanFormatter);
	}

	public LocalDate getAdmitDate() {
		return this.admitDate;
	}

	// Getters and setters (DischargeDate)
	public void setDischargeDate(String SDischargeDate) {
		this.dischargeDate = LocalDate.parse(SDischargeDate, germanFormatter);
	}

	public LocalDate getDischargeDate() {
		return this.dischargeDate;
	}

	// Override
	@Override
	public String toString() {
		return "InPatient [" + this.getName() + ", " 
							+ this.getBirthdate() + ", " 
							+ this.getGender() + ", "
							+ this.getWeight() + " kg. , " 
							+ this.getHeight() + " cm. admitDate=" 
							+ this.getAdmitDate()+ ", dischargeDate=" 
							+ this.getDischargeDate() + "]";
	}

}
