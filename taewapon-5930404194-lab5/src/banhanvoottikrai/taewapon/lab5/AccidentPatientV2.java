package banhanvoottikrai.taewapon.lab5;

import banhanvoottikrai.taewapon.lab4.*;

public class AccidentPatientV2 extends PatientV3 implements HasInsurance,UnderLegalAge {

		//Private variable
		private String name;
		private String typeOfAccident;
		private boolean isInICU;
		
		//Constructor
		public AccidentPatientV2(String SName, String SBirthdate, Gender SGender, double SWeight, int SHeight, String STypeOfAccident, boolean SisInICU) {
			super(SName, SBirthdate, SGender, SWeight, SHeight);
			this.name = SName;
			this.typeOfAccident = STypeOfAccident;
			this.isInICU = SisInICU;
		}
		

		//Getters and setters (TypeOfAccident)
		public String getTypeOfAccident() {
			return typeOfAccident;
		}
		public void setTypeOfAccident(String typeOfAccident) {
			this.typeOfAccident = typeOfAccident;
		}
		
		//Getters and setters (isInICU)
		public boolean isInICU() {
			return isInICU;
		}
		public void setInICU(boolean isInICU) {
			this.isInICU = isInICU;
		}
		
		//Getters and setters (Name)
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
		//Override
		@Override
		public String toString() {
			return "AccidentPatient [" 
					+ getTypeOfAccident() + ", "
					+ (isInICU ? "In ICU" : "is out of ICU") + "], Patient ["
					+ getName()+ ", " 
					+ getBirthdate() + ", " 
					+ getGender() + ", " 
					+ getWeight()+ " kg., " 
					+ getHeight() + "cm. ]";
		}
		public void patientReport(){
			System.out.println("You were in an accident.");
		}
		
		// new override
		public void pay() {
			System.out.println("pay the accident bill with insurrance");
		}

		public void pay(double amount){
			System.out.println("pay " + amount + " baht for his hospital visit by insurance ");
		}
		
		public void askPermission() {
			System.out.println("ask parents for permission to cure him in an accident");
		}
		
		public void askPermission(String legalGuardianName) {
			System.out.println("ask " + legalGuardianName + " for permission to treat with chemo");
		}
	}

