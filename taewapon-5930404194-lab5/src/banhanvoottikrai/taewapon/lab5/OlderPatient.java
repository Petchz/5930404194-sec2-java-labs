package banhanvoottikrai.taewapon.lab5;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

// import from lab4
import banhanvoottikrai.taewapon.lab4.*;

public class OlderPatient {
	public static void main(String arg[]){
		PatientV2 piti = new AccidentPatient("piti", "12.01.2000", Gender.MALE, 65.5, 169 , "Car accident", true);
		PatientV2 weera = new TerminalPatient("weera", "15.02.2000", Gender.MALE, 72, 172 , "Cancer", "01.01.2017");
		PatientV2 duangjai = new VIPPatient("duangjai", "21.05.2001", Gender.FEMALE, 47.5, 154 , 1000000, "mickeymouse");
		AccidentPatient petch = new AccidentPatient("petch", "15.10.1999", Gender.MALE, 68, 170, "FIre accident", true );
		
		isOlder(piti, weera);
		isOlder(piti, duangjai);
		isOlder(piti, petch);
		isOldest(piti, weera, duangjai);
		isOldest(piti, duangjai , petch);
	}
	
	// isOlder methods
	public static void isOlder(PatientV2 person1,PatientV2 person2){
		
		LocalDate birthdatePerson1 = person1.getBirthdate(); 
		LocalDate birthdatePerson2 = person2.getBirthdate();
		LocalDate today = LocalDate.now();
		long agePerson1 = birthdatePerson1.until(today, ChronoUnit.YEARS);
		long agePerson2 = birthdatePerson2.until(today, ChronoUnit.YEARS);

		if (agePerson1 > agePerson2 ){
			System.out.println(((person1 instanceof VIPPatient)? ((VIPPatient)person1).getVIPName("mickeymouse") : person1.getName()) + " is older than " + ((person2 instanceof VIPPatient)? ((VIPPatient)person2).getVIPName("mickeymouse") : person2.getName())); 
		}
		else {
			System.out.println(((person1 instanceof VIPPatient)? ((VIPPatient)person1).getVIPName("mickeymouse") : person1.getName()) + " is not older than " + ((person2 instanceof VIPPatient)? ((VIPPatient)person2).getVIPName("mickeymouse") : person2.getName()));
		}
	}

	// isOldest methods
	public static void isOldest(PatientV2 person1, PatientV2 person2, PatientV2 person3){
		
		LocalDate birthdatePerson1 = person1.getBirthdate(); 
		LocalDate birthdatePerson2 = person2.getBirthdate();
		LocalDate birthdatePerson3 = person3.getBirthdate();
		LocalDate today = LocalDate.now();
		long agePerson1 = birthdatePerson1.until(today, ChronoUnit.YEARS);
		long agePerson2 = birthdatePerson2.until(today, ChronoUnit.YEARS);
		long agePerson3 = birthdatePerson3.until(today, ChronoUnit.YEARS);
		
		if((agePerson1 > agePerson2) && agePerson1 > agePerson3){
			System.out.println(((person1 instanceof VIPPatient)? ((VIPPatient)person1).getVIPName("mickeymouse") : person1.getName()) + " is oldest among " + ((person1 instanceof VIPPatient)? ((VIPPatient)person1).getVIPName("mickeymouse") : person1.getName()) + ", " + ((person2 instanceof VIPPatient)? ((VIPPatient)person2).getVIPName("mickeymouse") : person2.getName()) + " and " + ((person3 instanceof VIPPatient)? ((VIPPatient)person3).getVIPName("mickeymouse") : person3.getName()));
		}
		else if(agePerson2 > agePerson3){ 
			System.out.println(((person2 instanceof VIPPatient)? ((VIPPatient)person2).getVIPName("mickeymouse") : person2.getName()) + " is oldest among " + ((person1 instanceof VIPPatient)? ((VIPPatient)person1).getVIPName("mickeymouse") : person1.getName()) + ", " + ((person2 instanceof VIPPatient)? ((VIPPatient)person2).getVIPName("mickeymouse") : person2.getName()) + " and " + ((person3 instanceof VIPPatient)? ((VIPPatient)person3).getVIPName("mickeymouse") : person3.getName()));
		}
		else {
			System.out.println(((person3 instanceof VIPPatient)? ((VIPPatient)person3).getVIPName("mickeymouse") : person3.getName()) + " is oldest among " + ((person1 instanceof VIPPatient)? ((VIPPatient)person1).getVIPName("mickeymouse") : person1.getName()) + ", " + ((person2 instanceof VIPPatient)? ((VIPPatient)person2).getVIPName("mickeymouse") : person2.getName()) + " and " + ((person3 instanceof VIPPatient)? ((VIPPatient)person3).getVIPName("mickeymouse") : person3.getName()));
		}
	}
}

