package banhanvoottikrai.taewapon.lab5;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import banhanvoottikrai.taewapon.lab4.*;
public class TerminalPatientV2 extends PatientV3 implements HasInsurance,UnderLegalAge {

	// Private variable
	private String name;
	private String terminalDisease;
	private LocalDate firstDiagnosed;
	
	// DateTimeformatter
	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMAN);

	// Constructor
	public TerminalPatientV2(String SName, String SBirthdate, Gender SGender, double SWeight, int SHeight, String SterminalDisease, String SfirstDiagnosed) {
		super(SName, SBirthdate, SGender, SWeight, SHeight);
		this.name = SName;
		this.terminalDisease = SterminalDisease;
		this.firstDiagnosed = LocalDate.parse(SfirstDiagnosed, germanFormatter);
	}


	// Getters and Setters (TerminalDisease)
	public String getTerminalDisease() {
		return terminalDisease;
	}

	public void setTerminalDisease(String terminalDisease) {
		this.terminalDisease = terminalDisease;
	}

	// Getters and Setters (FirstDiagnosed)
	public LocalDate getFirstDiagnosed() {
		return firstDiagnosed;
	}

	public void setFirstDiagnosed(LocalDate firstDiagnosed) {
		this.firstDiagnosed = firstDiagnosed;
	}

	// Override
	@Override
	public String toString() {
		return "TerminalPatient [" + terminalDisease + ", "
					+ firstDiagnosed + "], Patient ["
					+ getName()+ ", " 
					+ getBirthdate() + ", " 
					+ getGender() + ", " 
					+ getWeight()+ " kg., " 
					+ getHeight() + "cm. ]";
	}

	public void patientReport() {
		System.out.println("You have terminal illness.");
	}

	//new methods
	public String getName() {
		return name;
	}
	
	public void pay() {
		System.out.println("pay the accident bill with insurrance");
	}

	public void pay(double amount){
		System.out.println("pay " + amount + " baht for his hospital visit by insurance ");
	}
	
	public void askPermission() {
		System.out.println("ask parents for permission to cure him in an accident");
	}
	
	public void askPermission(String legalGuardianName) {
		System.out.println("ask " + legalGuardianName + " for permission to treat with chemo");
	}
	


}
