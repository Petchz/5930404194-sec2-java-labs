package banhanvoottikrai.taewapon.lab5;

import banhanvoottikrai.taewapon.lab4.*;

public class AccidentPatient extends PatientV2 {
	

	//Private variable
	private String typeOfAccident;
	private boolean isInICU;
	
	//Constructor
	public AccidentPatient(String SName, String SBirthdate, Gender SGender, double SWeight, int SHeight, String STypeOfAccident, boolean SisInICU) {
		super(SName, SBirthdate, SGender, SWeight, SHeight);
		this.typeOfAccident = STypeOfAccident;
		this.isInICU = SisInICU;
	}
	

	//Getters and setters (TypeOfAccident)
	public String getTypeOfAccident() {
		return typeOfAccident;
	}
	public void setTypeOfAccident(String typeOfAccident) {
		this.typeOfAccident = typeOfAccident;
	}
	
	//Getters and setters (isInICU)
	public boolean isInICU() {
		return isInICU;
	}
	public void setInICU(boolean isInICU) {
		this.isInICU = isInICU;
	}
	
	//Override
	@Override
	public String toString() {
		return "AccidentPatient [" 
				+ getTypeOfAccident() + ", "
				+ (isInICU ? "In ICU" : "is out of ICU") + "], Patient ["
				+ getName()+ ", " 
				+ getBirthdate() + ", " 
				+ getGender() + ", " 
				+ getWeight()+ " kg., " 
				+ getHeight() + "cm. ]";
	}
	public void patientReport(){
		System.out.println("You were in an accident.");
	}
	
}
