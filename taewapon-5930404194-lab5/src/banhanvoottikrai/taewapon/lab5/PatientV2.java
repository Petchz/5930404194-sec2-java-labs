package banhanvoottikrai.taewapon.lab5;

import banhanvoottikrai.taewapon.lab4.*;

public class PatientV2 extends Patient {
	
	//Constructor
	public PatientV2(String SName, String SBirthdate, Gender SGender, double SWeight, int SHeight) {
		super(SName, SBirthdate, SGender, SWeight, SHeight);
	}
		
	//Method
	public void patientReport(){
		System.out.println("You need medical attention.");
	}
	
}
