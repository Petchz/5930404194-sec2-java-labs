package banhanvoottikrai.taewapon.lab5;

import banhanvoottikrai.taewapon.lab4.*;

public class VIPPatientV2 extends PatientV3 implements HasInsurance,HasPet {

	// Private Variable
	private double totalDonation;
	private String passPhrase;

	// Constructor (encrypt "name")
	public VIPPatientV2(String SName, String SBirthdate, Gender SGender, double SWeight, int SHeight, int SDonate, String SMessage) {
		super(encrypt(SName), SBirthdate, SGender, SWeight, SHeight);
		this.totalDonation = SDonate;
		this.passPhrase = SMessage;
	}

	// Getters and Setters (TotalDonation)
	public double getTotalDonation() {
		return totalDonation;
	}

	public void setTotalDonation(double totalDonation) {
		this.totalDonation = totalDonation;
	}

	// Donate methods
	public double donate(double SDonate) {
		return totalDonation = totalDonation + SDonate;
	}

	// ENCRYPT
	private static  String encrypt(String SMessage) {
		String ECmassage = "";
		char tempEC[] = SMessage.toCharArray();
		for (int i = 0; i < SMessage.length(); i++) {

			tempEC[i] += 7;
			if (tempEC[i] > 122)
				tempEC[i] -= 26;

			ECmassage += tempEC[i];
		}
		
		return ECmassage;
		
	}

	// DECRYPT
	private String decrypt(String SMessage) {

		String DCmessage = "";
		char tempDC[] = SMessage.toCharArray();

		for (int i = 0; i < SMessage.length(); i++) {

			tempDC[i] -= 7;
			if (tempDC[i] < 97)
				tempDC[i] += 26;

			DCmessage += tempDC[i];
		}

		return DCmessage;
	}

	// getVIPName methods
	public String getVIPName(String SMessage) {

		if (SMessage.equals(passPhrase)) {
			return decrypt(getName());
		} else {
			return "Wrong passphrase, please try again";
		}


	}

	@Override
	public String toString() {
		return "VIPPatient [" + getTotalDonation() + ", Patient ["
						+ getName() + ", " 
						+ getBirthdate() + ", "
						+ getGender() + ", " 
						+ getWeight() + " kg., " 
						+ getHeight() + "cm. ]";
	}
	public void patientReport(){
		System.out.println("Your record is private.");
	}

	// new methods
	public void pay() {
		System.out.println("pay the accident bill with insurrance");
	}

	public void pay(double amoung){
		System.out.println("pay " + amoung + " baht for his hospital visit by insurance ");
	}
	
	public void feedPet(){
		System.out.println("feed the pet");
	}
	
	public void playWithPet() {
		System.out.println("play with pet.");
	}
}
