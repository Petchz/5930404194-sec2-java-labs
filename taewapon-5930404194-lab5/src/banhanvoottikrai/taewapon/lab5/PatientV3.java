package banhanvoottikrai.taewapon.lab5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import banhanvoottikrai.taewapon.lab4.*;

public class PatientV3 extends SickPeople {

		//Variable
		private String name;
		private LocalDate birthdate;
		private Gender gender;
		private double weight;
		private int height;
		
		// DatetimeFormatter
		DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
		
		// Constructor
		public PatientV3(String SName, String SBirthdate, Gender SGender, double SWeight, int SHeight) {
			super();
			this.name = SName;
			this.birthdate = LocalDate.parse(SBirthdate, germanFormatter);
			this.gender = SGender;
			this.weight = SWeight;
			this.height = SHeight;
		}

		// Getters and setters (Name)
		public String getName() {
			return name;
		} 

		public void setName(String SName) {
			this.name = SName;
		}

		// Getters and setters (Birthdate)
		public LocalDate getBirthdate() {
			return birthdate;
		}

		public void setBirthdate(LocalDate SBirthdate) {
			this.birthdate = SBirthdate;
		}

		// Getters and setters (Gender)
		public Gender getGender() {
			return gender;
		}

		public void setGender(Gender SGender) {
			this.gender = SGender;
		}

		// Getters and setters (Weight)
		public double getWeight() {
			return weight;
		}

		public void setWeight(double SWeight) {
			this.weight = SWeight;
		}

		// Getters and setters (Height)
		public int getHeight() {
			return height;
		}

		public void setHeight(int SHeight) {
			this.height = SHeight;
		}
		
		// Override
		@Override
		public String toString() {
			return "Patient [" + name + ", " + birthdate + ", " + gender + ", " + weight + " kg. , " + height +  " cm.]";
		}
}


