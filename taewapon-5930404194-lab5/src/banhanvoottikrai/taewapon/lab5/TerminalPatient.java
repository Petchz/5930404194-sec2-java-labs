package banhanvoottikrai.taewapon.lab5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import banhanvoottikrai.taewapon.lab4.Gender;

public class TerminalPatient extends PatientV2 {

	// Private variable
	private String terminalDisease;
	private LocalDate firstDiagnosed;
	
	// DateTimeformatter
	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMAN);

	// Constructor
	public TerminalPatient(String SName, String SBirthdate, Gender SGender, double SWeight, int SHeight, String SterminalDisease, String SfirstDiagnosed) {
		super(SName, SBirthdate, SGender, SWeight, SHeight);
		this.terminalDisease = SterminalDisease;
		this.firstDiagnosed = LocalDate.parse(SfirstDiagnosed, germanFormatter);
	}


	// Getters and Setters (TerminalDisease)
	public String getTerminalDisease() {
		return terminalDisease;
	}

	public void setTerminalDisease(String terminalDisease) {
		this.terminalDisease = terminalDisease;
	}

	// Getters and Setters (FirstDiagnosed)
	public LocalDate getFirstDiagnosed() {
		return firstDiagnosed;
	}

	public void setFirstDiagnosed(LocalDate firstDiagnosed) {
		this.firstDiagnosed = firstDiagnosed;
	}

	// Override
	@Override
	public String toString() {
		return "TerminalPatient [" + terminalDisease + ", "
					+ firstDiagnosed + "], Patient ["
					+ getName()+ ", " 
					+ getBirthdate() + ", " 
					+ getGender() + ", " 
					+ getWeight()+ " kg., " 
					+ getHeight() + "cm. ]";
	}

	public void patientReport() {
		System.out.println("You have terminal illness.");
	}

}
