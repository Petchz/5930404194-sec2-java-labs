package banhanvoottikrai.taewapon.lab5;

public interface HasPet {
	
	// two methods 
	public void feedPet();
	public void playWithPet();
}
