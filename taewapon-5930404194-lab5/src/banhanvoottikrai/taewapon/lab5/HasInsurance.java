package banhanvoottikrai.taewapon.lab5;

public interface HasInsurance {
	
	// two methods
	public void pay();
	public void pay(double amount);
}
