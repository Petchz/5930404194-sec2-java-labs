package banhanvoottikrai.taewapon.lab5;

public interface UnderLegalAge {
	
	// two methods 
	public void askPermission();
	public void askPermission(String legalGuardianName);
}