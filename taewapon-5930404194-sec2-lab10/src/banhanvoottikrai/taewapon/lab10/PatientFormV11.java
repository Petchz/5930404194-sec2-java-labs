package banhanvoottikrai.taewapon.lab10;

import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.SwingUtilities;

import banhanvoottikrai.taewapon.lab4.*;

public class PatientFormV11 extends PatientFormV10 implements Comparable<Patient>  {

	// generated UID
	private static final long serialVersionUID = 5698009611164746028L;

	
	// actionPerformed method
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == sortMenuItem) {
			sortPatients();
			displayPatients();
		}
	}
	
	// addListeners method
	public void addListeners() {
		super.addListeners();
		sortMenuItem.addActionListener(this);
	}
	
	// sortPatients method
	public void sortPatients() {
		Collections.sort(patientArrayList, PatientFormV11.WeightComparator);
		
	}
	
	// compareTo method
	public int compareTo(Patient p) {
		return this.nameTextfield.getText().compareTo(p.getName());
	}
	
	// Comparator method
	public static Comparator<Patient> WeightComparator = new Comparator<Patient>() {
		public int compare(Patient p1, Patient p2) {
			return (int) (p1.getWeight() - p2.getWeight());
		}
	};

	// title constructor
	public PatientFormV11(String title) {
		super(title);
	}
	
	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	// createAndShowGUI method
	public static void createAndShowGUI() {
		PatientFormV11 patientV11 = new PatientFormV11("PatientFormV11");
		patientV11.addComponents();
		patientV11.setFrameFeatures();
		patientV11.addListeners();
	}
}
	

