package banhanvoottikrai.taewapon.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;

import banhanvoottikrai.taewapon.lab4.*;
import banhanvoottikrai.taewapon.lab9.*;

public class PatientFormV10 extends PatientFormV9 implements ActionListener{

	// generated UID
	private static final long serialVersionUID = -2562322239619479418L;
	
	// declare variable
	protected JMenu dataMenu;
	protected JMenuItem displayMenuItem, sortMenuItem, searchMenuItem, removeMenuItem;
	protected ArrayList<Patient> patientArrayList = new ArrayList<Patient>();

	
	// variable from Patient
	protected String patientV10Name,patientV10BD;
	protected int patientV10Height;
	protected double patientV10Weight;
	protected Gender patientV10Gender;
	
	@Override
	public void addComponents() {
		super.addComponents();
		
		// setup for JMenu and JMenuItem
		dataMenu = new JMenu("Data");
		displayMenuItem = new JMenuItem("Display");
		sortMenuItem = new JMenuItem("Sort");
		searchMenuItem = new JMenuItem("Search");
		removeMenuItem = new JMenuItem("Remove");
		
		// add JMenuItem to JMenu
		dataMenu.add(displayMenuItem);
		dataMenu.add(sortMenuItem);
		dataMenu.add(searchMenuItem);
		dataMenu.add(removeMenuItem);
		
		// add JMenu to JMenuBar
		patientMenuBar.add(dataMenu);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == okButton) {
			addPatient();
		} else if (src == displayMenuItem) {
			displayPatients();
		}
	}
	
	@Override
	public void addListeners() {
		super.addListeners();
		
		// addAcionListener for displayMenuItem
		displayMenuItem.addActionListener(this);
	}
	
	// addPaient method
	public void addPatient() {
		
		patientV10Name = nameTextfield.getText();
		patientV10BD = birthdateTextfield.getText();
		patientV10Gender = (maleRadioButton.isSelected() ? Gender.MALE: Gender.FEMALE);
		patientV10Weight = Double.parseDouble(weightTextfield.getText());
		patientV10Height = Integer.parseInt(heightTextfield.getText());
	
		Patient patientV10List = new Patient(patientV10Name, patientV10BD, patientV10Gender, patientV10Weight,patientV10Height);
		
		// add Patient to ArrayList
		patientArrayList.add(patientV10List);
	}
	
	// displayPatients method
	public void displayPatients() {
		
		String message = "";
		
		for (int i=0; i < patientArrayList.size(); i++) {
			message += (i+1) + ": " + patientArrayList.get(i).toString() + "\n";
		}
		
		JOptionPane.showMessageDialog(this,message);
	}
	
	// title constructor
	public PatientFormV10(String title) {
		super(title);
	}
	
	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		PatientFormV10 patientForm10 = new PatientFormV10("Patient Form V10");
		patientForm10.addComponents();
		patientForm10.setFrameFeatures();
		patientForm10.addListeners();
	}
	
}

