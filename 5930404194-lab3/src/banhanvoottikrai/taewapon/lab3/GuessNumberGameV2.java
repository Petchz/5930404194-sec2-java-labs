package banhanvoottikrai.taewapon.lab3;

import java.util.Scanner;

public class GuessNumberGameV2 {
	
	static int remainingGuess;
	static int answer;

	public static void main(String[] args) {
		
		playgame() ;
		System.out.println("Want to play again (\"Y\" or \"y\" )");
		Scanner input = new Scanner(System.in);
		String playagain = input.nextLine();
		
		while(true) {
		if (playagain.equalsIgnoreCase("y")){
			playgame();
		} else
			System.exit(0);
		}
		
	}

	public static void playgame() {
		
		System.out.print("Enter min and max of random numbers: ");
		Scanner inputrandomnumber = new Scanner(System.in);
		int min = inputrandomnumber.nextInt();
		int max = inputrandomnumber.nextInt();
		
		System.out.print("Enter the number possible guess: ");
		Scanner inputremainingguess = new Scanner(System.in);
		remainingGuess = inputremainingguess.nextInt();
		answer = (int) (Math.random() * ((max - min) + 1));
		
		
		while (remainingGuess > 0) {
			
			System.out.println("Number of remaining guess is " + remainingGuess);
			System.out.print("Enter a guess: ");
			Scanner inputnumber = new Scanner(System.in);
			int guessnumber = inputnumber.nextInt();
			remainingGuess--;

			if (guessnumber == answer) {
				System.out.println("Correct!");
				System.exit(0);
			} else if (guessnumber < answer) {
				System.out.println("Higher!");
			} else if (guessnumber > answer) {
				System.out.println("Lower!");
			}
		}
		System.out.println("You ran out of guesses. The number was " + answer);
	}
}