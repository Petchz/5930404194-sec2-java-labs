package banhanvoottikrai.taewapon.lab3;

import java.util.Scanner;

public class GuessNumberGame {

	public static void main(String[] args) {

		int remainingGuess = 7;
		int answer = (int) (Math.random() * ((100 - 0) + 1));

		while (remainingGuess > 0) {
			System.out.println("Number of remaining guess is " + remainingGuess);
			System.out.print("Enter a guess: ");
			Scanner inputnumber = new Scanner(System.in);
			int guessnumber = inputnumber.nextInt();
			remainingGuess--;

			if (guessnumber == answer) {
				System.out.println("Correct!");
				
				System.exit(0);
			} else if (guessnumber < answer) {
				System.out.println("Higher!");
			} else if (guessnumber > answer) {
				System.out.println("Lower!");
			}
		}
		System.out.println("You ran out of guesses. The number was " + answer);
	}
}