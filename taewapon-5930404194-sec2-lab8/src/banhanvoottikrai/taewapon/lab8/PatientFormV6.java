package banhanvoottikrai.taewapon.lab8;

/**
 * PatientFormV6 Program
 * 
 * @author Taewapon Banhanvoottikrai 593040419-4
 */

import java.awt.*;
import javax.swing.*;

public class PatientFormV6 extends PatientFormV5{

	// generated UID
	private static final long serialVersionUID = 5857410056212303635L;
	
	// declare variable
	protected JPanel patientv6Panel,picPanel;
	protected JLabel v6pic;
	
	// create imageIcon
	ImageIcon iconOpen = new ImageIcon("bin/banhanvoottikrai/taewapon/lab8/openIcon.png");
	ImageIcon iconSave = new ImageIcon("bin/banhanvoottikrai/taewapon/lab8/saveIcon.png");
	ImageIcon iconExit = new ImageIcon("bin/banhanvoottikrai/taewapon/lab8/quitIcon.png");
	ImageIcon manee = new ImageIcon("bin/banhanvoottikrai/taewapon/lab8/manee.jpg");

	//title constructor
	public PatientFormV6(String title) {
		super(title);
	}

	// addComponents method
	public void addComponents() {
		super.addComponents();

		patientv6Panel = new JPanel(new BorderLayout());
		picPanel = new JPanel();
		
		v6pic = new JLabel(manee);
		
		// add Picture to pic panel
		picPanel.add(v6pic);

		patientv6Panel.add(patientMenuBar, BorderLayout.NORTH);
		patientv6Panel.add(picPanel, BorderLayout.CENTER);
		patientv6Panel.add(patientv3Panel, BorderLayout.SOUTH);
		add(patientv6Panel);

		// add Icon to MenuItem
		openMenuItem.setIcon(iconOpen);
		saveMenuItem.setIcon(iconSave);
		exitMenuItem.setIcon(iconExit);
	}
	
	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		PatientFormV6 patientForm6 = new PatientFormV6("Patient Form V6");
		patientForm6.addComponents();
		patientForm6.setFrameFeatures();
		patientForm6.addListeners();
	}
	
}