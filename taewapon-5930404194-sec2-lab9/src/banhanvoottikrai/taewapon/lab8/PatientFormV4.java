package banhanvoottikrai.taewapon.lab8;

/**
 * PatientFormV4 Program
 * 
 * @author Taewapon Banhanvoottikrai 593040419-4
 */

import java.awt.event.*;
import javax.swing.*;
import banhanvoottikrai.taewapon.lab6.*;

public class PatientFormV4 extends PatientFormV3 implements ActionListener{

	// genarated serial UID
	private static final long serialVersionUID = -1791844167694811261L;

	// title constructor
	public PatientFormV4(String title) {
		super(title);
	}
	
	// actionPerformed
	@Override
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		if (src == okButton) {
			handleOKButton();
		} else if (src == cancelButton) {
			handleCancelButton();
		} else if (src == patienttypeComboBox){
			handlePatientTypeComboBox();
		} else if (src == maleRadioButton){
			handleMaleRadioButton();
		} else if (src == femaleRadioButton)
			handleFemaleRadioButton();
	}

	// addListeners method
	protected void addListeners() {
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		patienttypeComboBox.addActionListener(this);
		maleRadioButton.addActionListener(this);
		femaleRadioButton.addActionListener(this);
	}

	//	handleOKButton
	protected void handleOKButton() {
		JOptionPane.showMessageDialog(this, "Name = " + nameTextfield.getText() + " " + 
				"Birthdate = " + birthdateTextfield.getText() + " " +
				"Weight = " + weightTextfield.getText() + " " +
				"Height = " + heightTextfield.getText() +  "\n" +
				"Gender = " + (maleRadioButton.isSelected() ? "Male" : 
								femaleRadioButton.isSelected() ? "Female":"") + "\n" +
				"Address = " + addressTextArea.getText() + "\n" +
				"Type = " + patienttypeComboBox.getSelectedItem());
	}

	//	handleCancelButton
	protected void handleCancelButton() {
		nameTextfield.setText("");
		birthdateTextfield.setText("");
		weightTextfield.setText("");
		heightTextfield.setText("");
		addressTextArea.setText("");
	}
	
	// handlePatientComboBox
	protected void handlePatientTypeComboBox() {
		if (patienttypeComboBox.getSelectedItem().equals("Inpatient")){
			JOptionPane.showMessageDialog(this, "Your patient type is now changed to Inpatient");
		}else if (patienttypeComboBox.getSelectedItem().equals("Outpatient")){
			JOptionPane.showMessageDialog(this, "Your patient type is now changed to Outpatient");	
		}

	}
	
	// handleMaleRadioButton
	 public void handleMaleRadioButton() {
		JOptionPane pane = new JOptionPane("Your Gender type is now changed to Male");
        JDialog jDialog = pane.createDialog(this, "Gender Info");
        jDialog.setLocation( getX(), getY() + getHeight() + 20);
        jDialog.setVisible( true );
	}
	
	// handleFemaleRadioButton
	public void handleFemaleRadioButton() {
		JOptionPane pane = new JOptionPane("Your Gender type is now changed to Female");
        JDialog jDialog = pane.createDialog(this, "Gender Info");
        jDialog.setLocation( getX(), getY() + getHeight() + 20);
        jDialog.setVisible( true );
    }
	

	// createAndShowGUI method
	public static void createAndShowGUI() {
		PatientFormV4 patientForm4 = new PatientFormV4("Patient Form V4");
		patientForm4.addComponents();
		patientForm4.setFrameFeatures();
		patientForm4.addListeners();
	}
	
	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}