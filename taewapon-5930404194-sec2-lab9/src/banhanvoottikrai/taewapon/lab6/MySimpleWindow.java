package banhanvoottikrai.taewapon.lab6;

/**
 * MySimpleWindow Program
 * 
 * @author Taewapon Banhanvoottikrai 593040419-4
 */

import java.awt.*;
import javax.swing.*;


public class MySimpleWindow extends JFrame {

	// serial UID
	private static final long serialVersionUID = 181816243733315084L;

	// declare variable
	protected JButton okButton;
	protected JButton cancelButton;
	protected JPanel mswPanel;

	// title constructor
	public MySimpleWindow(String title) {
		super(title);
	}

	// addComponents method
	protected void addComponents() {
		
		// setup JPanel
		mswPanel = new JPanel();

		// setup JButton
		okButton = new JButton("OK");
		cancelButton = new JButton("Cancel");

		// add JButton to JPanel
		mswPanel.add(cancelButton);
		mswPanel.add(okButton);

		// add JPanel to Frame
		add(mswPanel);
	}

	// setFrameFeatures method
	public void setFrameFeatures() {
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}
}