package banhanvoottikrai.taewapon.lab6;

/**
 * PatientFormV3 Program
 * 
 * @author Taewapon Banhanvoottikrai 593040419-4
 */

import java.awt.*;
import javax.swing.*;

public class PatientFormV3 extends PatientFormV2 {

	// serial UID
	private static final long serialVersionUID = -6218304610160905579L; 
	
	// declare variable
	protected String patienttypeString[] = {"Inpatient", "Outpatient"};
	protected JPanel patienttypePanel,patientv3Panel;
	protected JLabel patienttypeLabel;
	protected JComboBox patienttypeComboBox;
	protected JMenuBar patientMenuBar;
	protected JMenu fileMenu, configMenu;
	protected JMenuItem  newMenuItem, openMenuItem, saveMenuItem, exitMenuItem, colorMenuItem, sizeMenuItem;

	// title constructor
	public PatientFormV3(String title) {
		super(title);
	}

	@Override
	protected void addComponents() {
		super.addComponents();
		
		// setup JMenu and JMenuItem
		fileMenu = new JMenu("File");
		newMenuItem = new JMenuItem("New");
		openMenuItem = new JMenuItem("Open");
		saveMenuItem = new JMenuItem("Save");
		exitMenuItem = new JMenuItem("Exit");
		configMenu = new JMenu("Config");
		colorMenuItem = new JMenuItem("Color");
		sizeMenuItem = new JMenuItem("Size");

		// setup JMenuBar (put JMenuItem in JMenu and then put JMenu in a JMenuBar)
		patientMenuBar = new JMenuBar();
		patientMenuBar.add(fileMenu);
		fileMenu.add(newMenuItem);
		fileMenu.add(openMenuItem);
		fileMenu.add(saveMenuItem);
		fileMenu.add(exitMenuItem);
		patientMenuBar.add(configMenu);
		configMenu.add(colorMenuItem);
		configMenu.add(sizeMenuItem);
		
		// setup JPanel for patienttypePanel and patientv3Panel
		patienttypePanel = new JPanel();
		patientv3Panel = new JPanel();
		
		// setup JComboBox
		patienttypeComboBox = new JComboBox(patienttypeString);
		patienttypeComboBox.setSelectedIndex(1);
        
		// setup Layout for patienttypePanel and patientv3Panel
		patienttypePanel.setLayout(new GridLayout(0,2));
		patientv3Panel.setLayout(new BorderLayout());

		// setup patienttypeLabel
		patienttypeLabel = new JLabel("Patient Type: ");

		// add patienttypeLabel and patienttypeComboBox to patienttypePanel
		patienttypePanel.add(patienttypeLabel);
		patienttypePanel.add(patienttypeComboBox);
		
		// add patientMMenuBar, patientv2Panel, pateinttypePanel to patientv3Panel
		patientv3Panel.add(patientMenuBar, BorderLayout.NORTH);
		patientv3Panel.add(patientv2Panel, BorderLayout.CENTER);
		patientv3Panel.add(patienttypePanel, BorderLayout.SOUTH);

		// add patientv3Panel to totalPanel
		totalPanel.add(patientv3Panel);
		
		// add totalPanel to Frame
		add(totalPanel);
		
	}

	// createAndShowGUI method
	public static void createAndShowGUI() {
		PatientFormV3 patientForm3 = new PatientFormV3("Patient Form V3");
		patientForm3.addComponents();
		patientForm3.setFrameFeatures();
	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}