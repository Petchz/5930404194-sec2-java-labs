package banhanvoottikrai.taewapon.lab2;

import java.util.Arrays;

public class IntegersSorter {
	
	public static void main(String[] args) {

		if (args.length == 1) {
			System.out.println("IntegerSorter <the number of integer to sort> <i1> <i2> ... <in>");
			System.exit(0);
		}

		int sort = Integer.parseInt(args[0]);
		int[] ary = new int[sort];

		for (int i = 0; i < sort; i++) {
			int number = Integer.parseInt(args[i + 1]);
			ary[i] = number;
		}

		System.out.print("Before Sorting: ");
		System.out.println(Arrays.toString(ary));

		Arrays.sort(ary);
		System.out.print("After Sorting: ");
		System.out.println(Arrays.toString(ary));
	}
}