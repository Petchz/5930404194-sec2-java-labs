package banhanvoottikrai.taewapon.lab2;

public class MoneyChanger {

	public static void main(String args[]) {

		int cost = Integer.parseInt(args[0]);
		int exchange = 1000 - cost;
		int bn500 = exchange / 500;
		int bn100 = (exchange % 500) / 100;
		int bn50 = ((exchange % 500) % 100) / 50;
		int bn20 = (((exchange % 500) % 100) % 50) / 20;
		

		System.out.println("You give 1000 for the cost as " + cost + " Baht.");

		if (cost > 1000) {
			System.out.println("No change.");
			System.exit(0);
		}
		
		System.out.println("You will recieve exchange as " + exchange + " Baht.");
		
		if (exchange > 0) {

			if (bn500 > 0) {
				System.out.print(bn500 + " of 500 bank note; ");
			}
			if (bn100 > 0) {
				System.out.print(bn100 + " of 100 bank note; ");
			}
			if (bn50 > 0) {
				System.out.print(bn500 + " of 50 bank note; ");
			}
			if (bn20 > 0) {
				System.out.print(bn20 + " of 20 bank note; ");
			}
		}
	}
}