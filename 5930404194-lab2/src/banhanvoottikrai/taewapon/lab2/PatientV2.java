package banhanvoottikrai.taewapon.lab2;

public class PatientV2 {

	public static void main(String[] args) {

		if (args.length != 4) {
			System.err.println("Patient <name> <gender> <weight> <height>");
			System.exit(0);
		}

		String name = args[0];
		String gender = args[1].toLowerCase();

		System.out.println("This patient name is " + name);

		float weight = Float.parseFloat(args[2]);
		if (weight < 0) {
			System.err.println("Weight must be non-negative");
			System.exit(0);
		}

		int height = Integer.parseInt(args[3]);
		if (height < 0) {
			System.err.println("Height must be non-negative");
			System.exit(0);
		}

		if (gender.equals("male")) {
			System.out.println("His weight is " + weight + " kg. and height is " + height + " cm.");
		}

		else if (gender.equals("female")) {
			System.out.println("Her weight is " + weight + " kg. and height is " + height + " cm.");
		}

		else {
			System.out.println("Please enter gender as only Male or Female");
		}

	}

}