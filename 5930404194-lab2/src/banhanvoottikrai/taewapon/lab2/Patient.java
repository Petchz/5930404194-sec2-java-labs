package banhanvoottikrai.taewapon.lab2;

public class Patient {

	public static void main(String[] args) {

		if (args.length != 4) {
			System.err.println("Patient <name> <gender> <weight> <height>");
			System.exit(0);
		}

		String name = args[0];
		String Female = "Female";
		String Male = "Male";
		float weight = Float.parseFloat(args[2]);
		int height = Integer.parseInt(args[3]);

		System.out.println("This patient name is " + name);

		if (args[1].equals(Male)) {
			System.out.println("His weight is " + weight + " kg. and height is " + height + " cm.");
		}

		else if (args[1].equals(Female)) {
			System.out.println("Her weight is " + weight + " kg. and height is " + height + " cm.");
		}

		else {
			System.out.println("Please enter gender as only Male or Female");
		}

	}

}
