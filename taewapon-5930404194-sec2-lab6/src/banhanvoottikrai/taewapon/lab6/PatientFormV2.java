package banhanvoottikrai.taewapon.lab6;

/**
 * PatientFormV2 Program
 * 
 * @author Taewapon Banhanvoottikrai 593040419-4
 */

import java.awt.*;
import javax.swing.*;


public class PatientFormV2 extends PatientFormV1 {

	// serial UID
	private static final long serialVersionUID = -6904864195813582179L;

	// declare variable
	protected JPanel genderPanel, radioButtonPanel, addressPanel, patientv2Panel;
	protected JLabel genderLabel, addressLabel;
	protected ButtonGroup genderRadioGroup;
	protected JRadioButton maleRadioButton, femaleRadioButton;
	protected JScrollPane addressScrollPane;
	protected JTextArea addressTextArea;

	// title constructor
	public PatientFormV2(String title) {
		super(title);
	}

	@Override
	protected void addComponents() {
		super.addComponents();

		// setup JPanel
		addressPanel = new JPanel();
		genderPanel = new JPanel();
		radioButtonPanel = new JPanel();
		patientv2Panel = new JPanel();
		
		// setup JLabel
		genderLabel = new JLabel("Gender: ");
		addressLabel = new JLabel("Address: ");
		
		// setup JButton and ButtonGroup
		genderRadioGroup = new ButtonGroup();
		maleRadioButton = new JRadioButton("Male");
		femaleRadioButton = new JRadioButton("Female");
		
		
		// setup JTextArea
		addressTextArea = new JTextArea("Department of Computer Engineering, Faculty of Engineering, "
				+ "Khon Kaen University, Mittraparp Rd., T. Naimuang, A. Muang, Khon Kaen, Thailand, 40002");
		addressTextArea.setLineWrap(true);
		addressTextArea.setWrapStyleWord(true);
		
		// setup JScrollPane
		addressScrollPane = new JScrollPane(addressTextArea);
		addressScrollPane.setPreferredSize(new Dimension(2, 35));

		// setup Layout for genderPanel, addressPanel and patientv2Panel
		genderPanel.setLayout(new GridLayout(0, 2));
		addressPanel.setLayout(new GridLayout(0, 1));
		patientv2Panel.setLayout(new BorderLayout());
		
		// add Button to Panel and buttonGroup
		radioButtonPanel.add(maleRadioButton);
		radioButtonPanel.add(femaleRadioButton);
		genderRadioGroup.add(maleRadioButton);
		genderRadioGroup.add(femaleRadioButton);

		// add Label and Button to genderPanel
		genderPanel.add(genderLabel);
		genderPanel.add(radioButtonPanel);

		// add Label and ScrollPane to addressPanel
		addressPanel.add(addressLabel);
		addressPanel.add(addressScrollPane);
		
		// add patientPanel, genderPanel and addressPanel to patientv2Panel
		patientv2Panel.add(patientPanel, BorderLayout.NORTH);
		patientv2Panel.add(genderPanel, BorderLayout.CENTER);
		patientv2Panel.add(addressPanel, BorderLayout.SOUTH);
		
		// add patientv2Panel to totalPanel
		totalPanel.add(patientv2Panel);
		
		// add totalPanel to Frame
		add(totalPanel);
	}

	// createAndShowGUI method
	public static void createAndShowGUI() {
		PatientFormV2 patientForm2 = new PatientFormV2("Patient Form V2");
		patientForm2.addComponents();
		patientForm2.setFrameFeatures();
	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}